import sys

lines = sys.stdin.read().strip().split('\n')

nbrSocks = int(lines[0])

originalPile = lines[1].split(' ')
nbrMoves = 0

socksTaken = 0

secondPile = []

while (socksTaken < nbrSocks and len(originalPile) != 0):
    sock = originalPile.pop(0)
    secondPile.insert(0, sock)
    nbrMoves += 1
    while (len(originalPile) > 0 and len(secondPile) > 0 and originalPile[0] == secondPile[0] and socksTaken < nbrSocks):
        originalPile.pop(0)
        secondPile.pop(0)
        nbrMoves += 1
        socksTaken += 1
    
if socksTaken < nbrSocks:
    print("impossible")
else:
    print(nbrMoves)