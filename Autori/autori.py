import sys

for line in sys.stdin:
    string = ''
    for char in line:
        if (char.isupper()):
            string += char
        
    print(string)